import React, {Component} from 'react';
import './Calculator.css';
import {connect} from 'react-redux';


class Calculator extends Component{

        getTotalSum() {
            return (this.props.number)
        }

        render() {
            console.log(this.props.number);
           let  numbers = [];
           for (let i = 0; i < 10; i++) {
               numbers.push(<button onClick={() => this.props.addNumber(i)}>{i}</button>);

           }

            return(
                <div className='Calculator'>
                <h2 className="Name">Calculator</h2>
                <p className="Total">{this.getTotalSum()}</p>
                    {numbers}
                <button onClick={() => this.props.sumNumber(numbers)} className="Plus">+</button>
                <button onClick={() => this.props.subNumber(numbers)} className="Sub">-</button>
                <button onClick={() => this.props.multiNumber(numbers)} className="Multi">*</button>
                <button onClick={() => this.props.divisionNumber(numbers)} className="Division">/</button>
                <button onClick={() => this.props.pointNumber(numbers)} className="Point">.</button>
                <button onClick={() => this.props.rightNumber(numbers)} className="Right">(</button>
                <button onClick={() => this.props.leftNumber(numbers)} className="Left">)</button>
                <button onClick={() => this.props.percentNumber(numbers)} className="Percent">%</button>
                <button onClick={() => this.props.cancelNumber(numbers)} className="Percent">C</button>
                <button onClick={() => this.props.calculateNumber(numbers)} className="Result">=</button>
                </div>
            )
        }
}
const mapStateToProps = state => {
    return {
        number: state.nums
    }
};

const mapDispatchToProps = dispatch => {
    return{
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        sumNumber: sum => dispatch({type: 'SUM', sum}),
        subNumber: sub => dispatch({type: 'SUBTRACTION', sub}),
        multiNumber: multi => dispatch({type: 'MULTIPLICATION', multi}),
        divisionNumber: division => dispatch({type: 'DIVISION', division}),
        pointNumber: point => dispatch({type: 'POINT', point}),
        leftNumber: left => dispatch({type: 'LEFT', left}),
        rightNumber: right => dispatch({type: 'RIGHT', right}),
        percentNumber: percent => dispatch({type: 'PERCENT', percent}),
        cancelNumber: cancel => dispatch({type: 'CANCEL', cancel}),
        calculateNumber: calculate => dispatch({type: 'CALCULATE', calculate})

    }
};



export default connect(mapStateToProps, mapDispatchToProps )(Calculator);